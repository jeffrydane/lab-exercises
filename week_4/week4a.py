#!/usr/bin/env python3
# Copyright 2012-13 Qtrac Ltd. All rights reserved.
# This program or module is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version. It is provided for
# educational purposes and is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# This program has been modified from its original source
# (stationery1, stationery2.py) to fit in Advanced Programming 2017
# week 4 exercise.

import abc
import sys
import functools
from week4 import SimpleItem, CompositeItem, make_item, make_composite


def main():
    pencil = make_item("Pencil", 0.40)
    ruler = make_item("Ruler", 1.60)
    eraser = make_item("Eraser", 0.20)
    pencilSet = make_composite("Pencil Set", pencil, ruler, eraser)
    box = make_item("Box", 1.00)
    boxedPencilSet = make_composite("Boxed Pencil Set", box,
                                    pencilSet)
    boxedPencilSet.add(pencil)
    for item in (pencil, ruler, eraser, pencilSet, boxedPencilSet):
        item.print()

if __name__ == "__main__":
    main()
