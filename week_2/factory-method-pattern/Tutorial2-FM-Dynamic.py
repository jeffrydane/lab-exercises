#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

class BoardStore:
	def orderBoard(self,type):
		boardFactory = BoardFactory()
		return boardFactory.createBoard(type)

class BoardFactory(object):
	def createBoard(self,type):
		if type=='3x3':
			board = Board3x3()
			print(board)
		if type == 'nxn':
			masukan = input('ukuran board:')
			n = int(masukan)
			board = Boardnxn(n)
			print(board)
		else:
			return None

def main():
	boardStore = BoardStore()
	boardStore.orderBoard("nxn")

class AbstractBoard:
	
	def __init__(self, rows, columns):
		self.board = [[None for _ in range(columns)] for _ in range(rows)]
		self.populate_board()

	def populate_board(self):
		raise NotImplementedError()

	def __str__(self):
		squares = []
		for x, row in enumerate(self.board):
			for y, column in enumerate(self.board):
				squares.append(self.board[x][y])
			squares.append("\n")
		return "".join(squares)

class Boardnxn(AbstractBoard):
	def __init__(self,n):
		self.n = n
		super().__init__(self.n, self.n)
		
	def populate_board(self):
		for row in range(self.n):
			for column in range(self.n):
				if (row % 2 == 0):
					if(column % 2):
						self.board[row][column] = create_piece("v")
					else:
						self.board[row][column] = create_piece("u")
				else:
					if (column % 2):
						self.board[row][column] = create_piece("o")
					else:
						self.board[row][column] = create_piece("x")

def create_piece(kind):
	if kind == "o":
		return Circle()
	elif kind == "x":
		return Cross()
	elif kind == "v":
		return V()
	elif kind == "u":
		return U()


# Mandatory 3: Uncomment the codes below
class Piece(str):
	__slots__ = ()
	
class Circle(Piece):
# Mandatory 3: Implement the Factory Method in here
	__slots__ = ()

	def __new__(Class):
		return super().__new__(Class, "o")

class Cross(Piece):
# Mandatory 3: Implement the Factory Method in here
	__slots__ = ()

	def __new__(Class):
		return super().__new__(Class, "x")

class U(Piece):
# Mandatory 3: Implement the Factory Method in here
	__slots__ = ()

	def __new__(Class):
		return super().__new__(Class, "u")

class V(Piece):
# Mandatory 3: Implement the Factory Method in here
	__slots__ = ()

	def __new__(Class):
		return super().__new__(Class, "v")

if __name__ == "__main__":
    main()
