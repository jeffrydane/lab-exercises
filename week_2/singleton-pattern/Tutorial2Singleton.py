#	Mandatory 9: Edit IdnCurrRates class to a Singleton class
#class IdnCurrRates (object):
#	def __init__(self,rates):
#		self.rates = rates
#	def __str__(self):
#		return str(self.rates)
class IdnCurrRates (object):
	class __IdnCurrRates:
		def __init__(self):
			self.rates = None
		def __str__(self):
			return str(self.rates)
	instance = None

	def __new__(cls):  # __new__ always a classmethod
		if not IdnCurrRates.instance:
			IdnCurrRates.instance = IdnCurrRates.__IdnCurrRates()
		return IdnCurrRates.instance

x = IdnCurrRates()
x.rates = 10000
print ('Rates x: '+ str(x))
y = IdnCurrRates()
y.rates = 20000
print ('Rates y: ' + str(y))
z = IdnCurrRates()
z.rates = 30000
print ('Rates z: ' + str(z))
print ('X and Y condition should be 30000')
print ('Rates x: '+ str(x))
print ('Rates y: ' + str(y))
