class OnlyOne:
    class __OnlyOne:
        def __init__(self, arg):
            self.val = arg
        def __str__(self):
            return repr(self) + self.val
    instance = None
    def __init__(self, arg):
        if not OnlyOne.instance:
            OnlyOne.instance = OnlyOne.__OnlyOne(arg)
            print("inisiasi sebuah instance")
        else:
            OnlyOne.instance.val = arg
            print("update di instance yang sama")
    def __getattr__(self, name):
        return getattr(self.instance, name)

    def __str__(self):
        return repr(self) + getattr(self,'val')

x = OnlyOne('sausage')
print(x)
print(x.instance)
print()
y = OnlyOne('eggs')
print(y)
print(y.instance)
print()
z = OnlyOne('spam')
print(z)
print(z.instance)
print()

print('Kembali melihat object x')
print(x)
print(x.instance)
print()

print('Kembali melihat object y')
print(y)
print(y.instance)
print()