import abc

class Shape(metaclass=abc.ABCMeta):

    def __init__(self,drawAPI):
        self._drawAPI = drawAPI

    def draw(self):
        pass

def main():
    redCircle = Circle(100,100, 10, RedCircle())
    greenCircle = Circle(100,100, 10, GreenCircle())

    redCircle.draw()
    greenCircle.draw()

class Circle(Shape):


    def __init__(self,x,y,radius,drawAPI):
        super().__init__(drawAPI)
        self.__x = x
        self.__y = y
        self.__radius = radius

    def draw(self):
        self._drawAPI.drawCircle(self.__radius, self.__x, self.__y)

class DrawAPI:

    def drawCircle(self, radius, x, y):
        pass

class RedCircle(DrawAPI):

    def drawCircle(self, radius, x, y):
        print("Drawing Circle[ color: red, radius: {}, x: {}, {}]".format(radius,x,y))

class GreenCircle(DrawAPI):

    def drawCircle(self, radius, x, y):
        print("Drawing Circle[ color: green, radius: {}, x: {}, {}]".format(radius, x, y))

if __name__ == "__main__":
    main()
