import abc

class UseCase(metaclass=abc.ABCMeta):
    def __init__(self,implementation):
        self._implementation = implementation

    def someFunctionality(self):
        pass

class OperatingSystem:
    def anotherFunctionality(self):
        pass

class LogOff(UseCase):
    def __init__(self, implementation):
        super().__init__(implementation)

    def someFunctionality(self):
        print("LogOff: ", self._implementation.anotherFunctionality())

class Shutdown(UseCase):
    def __init__(self, implementation):
        super().__init__(implementation)

    def someFunctionality(self):
        print("ShutDown ", self._implementation.anotherFunctionality())

class Ubuntu(OperatingSystem):
    def anotherFunctionality(self):
        return ("Ubuntu")


class Windows(OperatingSystem):
    def anotherFunctionality(self):
        return ("Windows")


def main():
    ubuntu = Ubuntu()
    windows = Windows()
    useCase = LogOff(ubuntu)
    # [1]
    useCase.someFunctionality()
    """
    Output for code [1]
    LogOff : Ubuntu
    """
    useCase = LogOff(windows)
    # [2]
    useCase.someFunctionality()
    """
    Output for code [2]
    LogOff : Windows
    """
    useCase = Shutdown(ubuntu)
    # [3]
    useCase.someFunctionality()
    """
    Output for code [3]
    Shutdown : Ubuntu
    """
    useCase = Shutdown(windows)
    # [4]
    useCase.someFunctionality()
    """
    Output for code [4] :
    Shutdown : Windows
    """
if __name__ == "__main__":
    main()